# README #

Early days, so this file just states some current goals. Cinch (it's a wordplay: easy, and sychronised!) wants to grow up to be a very easy to use Java library that provides a platform for doing cool creative things across multiple computers, such as:

* autodiscovery of servers and clients;
* tight clock synch and timestamped messaging;
* sending of all kinds of data and messages;
* super-simple deployment and updates across many machines;
* live code injection;
* remote management of full-screen interfaces;
* sharing data;
* audio and video coolness;
* webby things?
* collaborative creative environment?

It's not a grid computing platform. 

### Version ###

* Version 0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.1

### Who do I talk to? ###

* Repo owner: ollie@icarus.nu