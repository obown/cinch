package communication;

public interface Listener {
	
	public void message(Message message);
	
}