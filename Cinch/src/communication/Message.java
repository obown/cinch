package communication;

import core.AgentReference;

public class Message {
	AgentReference sender;
	Timestamp when;
	String key;
	Object value;
}