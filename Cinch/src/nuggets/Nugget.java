package nuggets;

import core.Agent;

public interface Nugget {
	
	public void run(Agent agent);

}
